#!/usr/bin/env node
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const path = require('path');

let args = yargs(hideBin(process.argv))
  .command(
    'run [directory]',
    'Finds all images and groups them by week created',
    (yargs) => {
      yargs.positional('directory', {
        describe:
          'Relative directory path to scrape for images, deafults to `.`',
        default: '.',
      });
    },
  )
  .option('debug', {
    alias: 'd',
    type: 'boolean',
    default: false,
    description: 'Run with verbose logging',
  })
  .option('glob', {
    alias: 'g',
    type: 'string',
    default: '*.{png,jpg}',
    description: 'Glob to use for searching for images',
  })
  .option('language', {
    alias: 'l',
    type: 'string',
    default: 'eng',
    description: 'Language to use when scraping images for text',
  })
  .option('move', {
    alias: 'm',
    type: 'boolean',
    default: true,
    description: 'Move images instead of copying them',
  })
  .option('format', {
    alias: 'f',
    type: 'string',
    default: 'MM-DD',
    description: 'What date format to use for the week range folder',
  }).argv;
const config = {
  archiveFolder: 'Screen Shot Archive',
  homeDir: path.join(process.cwd(), args.directory),
  dateFormat: args.format,
  glob: args.glob,
  moveImages: args.move,
  language: args.language,
  debug: args.debug,
};
require('./index')(config);
