const defaultConfig = require('./Config');
const Filesystem = require('./Filesystem');
const Dates = require('./Dates');
const ImageParser = require('./ImageParser');

async function main(config) {
  if (!config) {
    config = defaultConfig;
  }
  const logger = require('./Logger')(config);
  const filesystem = new Filesystem(config);
  const dates = new Dates(config);
  const imageParser = new ImageParser(filesystem, config, logger);
  await filesystem.ensureNewFolder();
  let images = await filesystem.getScreenshots();
  logger.log('info', 'Images to process:', images);
  for await (let screenshot of images) {
    if (screenshot.includes('.resize.png')) {
      logger.log(
        'debug',
        `Skipping due to it being a resized image from previous run: ${screenshot}`,
      );
      continue;
    }
    logger.log('info', `Working on: ${screenshot}`);
    let createdDate = await filesystem.getCreatedDate(screenshot);
    logger.log('debug', `Created on: ${createdDate}`);
    const ranges = dates.getWeekRange(createdDate.toISOString());
    logger.log('debug', `Ranges: ${ranges.toString()}`);
    const weekFolder = dates.getWeekFolder(ranges);
    logger.log('debug', `Folder name: ${weekFolder}`);
    await filesystem.createWeekFolder(ranges);
    const parsedText = await imageParser.parseImage(screenshot);
    await filesystem.writeOut(weekFolder, screenshot, parsedText);
    logger.log('info', `Finished working on: ${screenshot}`);
  }
  return images;
}

module.exports = main;
