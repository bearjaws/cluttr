module.exports = {
  archiveFolder: process.env.archive_folder || 'Screen Shot Archive',
  homeDir: process.env.directory || require('os').userInfo().homedir,
  dateFormat: process.env.date_format || 'MM-DD',
  glob: process.env.glob || `./Desktop/**/*.png`,
  moveImages: process.env.move_images !== 'false',
  language: 'eng',
  debug: false,
};
