const moment = require('moment');
class Dates {
  constructor(config) {
    this.config = config;
  }

  getWeekFolder(ranges) {
    return `${this.config.homeDir}/${this.config.archiveFolder}/${ranges.dates.year}/${ranges.start} thru ${ranges.end}`;
  }

  getWeekRange(date) {
    const parsed = moment(date);
    const dates = {
      year: parsed.year(),
      month: parsed.month(),
      day: parsed.day(),
    };

    // Need to clone here to prevent parsed from being reset with .startOf
    const startDate = parsed.clone().startOf('week');
    const endDate = parsed.clone().endOf('week');
    return {
      dates: dates,
      start: startDate.format(this.config.dateFormat),
      end: endDate.format(this.config.dateFormat),
    };
  }
}

module.exports = Dates;
