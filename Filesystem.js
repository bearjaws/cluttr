const mkdirp = require('mkdirp');
const fs = require('fs').promises;
const path = require('path');
const Dates = require('./Dates');
const glob = require('glob-promise');

class Filesystem {
  constructor(config) {
    this.config = config;
    this.dates = new Dates(this.config);
  }

  /**
   * Creates the foldler for grouping images by week
   * @param {object} ranges
   * @returns Promise
   */
  async createWeekFolder(ranges) {
    const path = this.dates.getWeekFolder(ranges);
    return mkdirp(path);
  }
  /**
   *
   * @param {string} weekFolder - Folder name grouped by week
   * @param {string} screenshot - name of screen shot
   * @param {string} text - The image text to save
   */
  async writeOut(weekFolder, screenshot, text) {
    const filename = path.basename(screenshot);
    const imagePath = this.getScreenshotPath(screenshot);
    const copyPath = `${weekFolder}/${filename}`;
    await fs.unlink(this.getResizeImagePath(screenshot));
    if (this.config.moveImages) {
      await fs.rename(imagePath, copyPath);
    } else {
      await fs.copyFile(imagePath, copyPath);
    }
    await fs.writeFile(
      `${weekFolder}/${filename.replace('.png', '.txt')}`,
      text,
      'utf8',
    );
  }

  /**
   * Creates the archive folder if needed
   * @returns Promise
   */
  async ensureNewFolder() {
    const path = `${this.config.homeDir}/${this.config.archiveFolder}`;
    console.log(path);
    return mkdirp(path);
  }

  /**
   * Gets the ctime (more reliable than birthdate) or mtime for the image
   * @param {string} screenshot
   * @returns Date
   */
  async getCreatedDate(screenshot) {
    const data = await fs.stat(this.getScreenshotPath(screenshot));
    return data.ctime || data.mtime;
  }

  /**
   * Gets the absoute path to an image
   * @param {string} screenshot
   * @returns string
   */
  getScreenshotPath(screenshot) {
    return path.join(this.config.homeDir, screenshot);
  }

  /**
   * Generates a resize image name
   * @param {string} screenshot
   * @returns string
   */
  getResizeImagePath(screenshot) {
    const screenshotPath = this.getScreenshotPath(screenshot);
    return `${screenshotPath}.resize.png`;
  }

  /**
   * Uses a glob to search for images
   * @returns String[] - Array of image names
   */
  async getScreenshots() {
    return await glob(this.config.glob, {
      cwd: this.config.homeDir,
      ignore: ['node_modules/**', this.config.archiveFolder],
    });
  }
}

module.exports = Filesystem;
