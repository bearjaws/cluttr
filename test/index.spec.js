const { describe } = require('mocha');
const { expect } = require('chai');
const fs = require('fs').promises;

process.env.directory = `${process.cwd()}/test/examples`;
process.env.glob = '*.png';
process.env.move_images = false;
const config = require('../Config');
const Dates = require('../Dates');
const dates = new Dates(config);
describe('Integration tests using env vars', async function () {
  before(async function () {
    // Gitlab shared runners are slow
    this.timeout(240 * 1000);
    try {
      await fs.rm(`${process.env.directory}/Screen Shot Archive/`, {
        recursive: true,
      });
    } catch (err) {
      // its okay if dir doesn't exist
      if (err.code !== 'ENOENT') {
        throw err;
      }
    }
    
    return require('../index')();
  });

  after(async ()=> {
    try {
      await fs.rm(`${process.env.directory}/Screen Shot Archive/`, {
        recursive: true,
      });
    } catch (err) {
      // its okay if dir doesn't exist
      if (err.code !== 'ENOENT') {
        throw err;
      }
    }
  })

  it('should create a folder named "Screen Shot Archive"', async function () {
    await fs.stat(`${process.env.directory}/Screen Shot Archive`);
  });

  it('should delete all the resized images', async function () {
    let result = await fs.readdir(`${process.env.directory}/`);
    for (let file of result) {
      expect(file.includes('.resize')).to.equal(false);
    }
  });

  it('should honor setting the move flag to false', async function () {
    const ranges = dates.getWeekRange(new Date());
    const folder = dates.getWeekFolder(ranges);
    let resultImages = await fs.readdir(
      folder,
    );
    let resultPngs = [];
    for (let file of resultImages) {
      if (file.includes('.png') === true) {
        resultPngs.push(file);
      }
    }

    // This checks that all the pngs in the example folder were processed and copied successfully
    let exampleFiles = await fs.readdir(`${process.env.directory}/`);
    expect(exampleFiles).to.not.be.empty;
    for (let file of exampleFiles) {
      if (file.includes('.png') === true) {
        expect(resultPngs.includes(file)).to.equal(true);
      }
    }
  });
});
