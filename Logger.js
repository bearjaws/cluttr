const winston = require('winston');

module.exports = (config) => {
  const logger = winston.createLogger({
    level: config.debug === false ? 'info' : 'debug',
    format: winston.format.json(),
  });
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );

  return logger;
};
