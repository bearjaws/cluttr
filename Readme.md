# Cluttr - Automatically organize your photos and make their text searchable using Finder / Windows Explorer

## Installation
```
npm i cluttr -g
cluttr --help
```

Cluttr automatically installs all dependencies, including tesseract.

*Only tested on MacOSX Mojave & Big Sur*
## Example Video
![](https://gitlab.com/bearjaws/cluttr/-/raw/master/readme_content/video_2.mp4)

[Video link](https://gitlab.com/bearjaws/cluttr/-/raw/master/readme_content/video_2.mp4)
## Running
By default cluttr will create a folder named "Screen Shot Archive" and move all your images into folders grouped by week.
Most of the options are configurable, see below.
```
cluttr --help
```

![](https://gitlab.com/bearjaws/cluttr/-/raw/master/readme_content/help.png)

```
cd ~/Desktop
cluttr run .
```
![](https://gitlab.com/bearjaws/cluttr/-/raw/master/readme_content/running.png)

Please note, tesseract can be pretty slow, despite downscaling images it can still take 1-2 minutes per image.

## Testing
```
npm test
```

