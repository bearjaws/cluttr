const Tesseract = require('tesseract.js');
const sharp = require('sharp');

class ImageParser {
  constructor(filesystem, config, logger) {
    this.config = config;
    this.logger = logger;
    this.filesystem = filesystem;
  }

  /**
   * Runs tesseract against the image to extract text
   * @param {string} screenshot
   * @returns
   */
  async parseImage(screenshot) {
    let parsedText = 'failed to parse';
    try {
      let screenshotPath = this.filesystem.getScreenshotPath(screenshot);
      let resizePath = this.filesystem.getResizeImagePath(screenshot);
      this.logger.log('debug', `Resize path: ${resizePath}`);
      // tesseract chokes on large images, so resizing to 1280 width helps with performance
      await sharp(screenshotPath).resize(1900).png().toFile(resizePath);
      this.logger.log('debug', 'Starting Tesseract');
      const {
        data: { text },
      } = await Tesseract.recognize(resizePath, this.config.language);
      this.logger.log('debug', 'Finished Tesseract');
      parsedText = text;
      this.logger.log('debug', `Found text ${parsedText}`);
    } catch (err) {
      this.logger.log('error', err.message);
    }

    return parsedText;
  }
}

module.exports = ImageParser;
